import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;


public class runner {
    public static String browserName = "Android";
    public static String VERSION = "5.1.1";
    public static String deviceName = "emulator-5554";
    public static String platformName = "Android";
    public static String appPath = getAbsolutePath("Entrymanager-debug.apk");
    public static String appPackage = "com.rockers.ticketing";
    public static String appActivity = "com.ticketembassy.entrymanager.Splash";
    public static String appiumServerIP = "0.0.0.0";
    public static String appiumServerPort = "4723";
    public static String userName = "dtester345@gmail.com";
    public static String password = "12345678";


    public static void main(String[] args) {
        System.out.println("Start device setup.......");
        WebDriver driver = null;
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("BROWSER_NAME", browserName);
        capabilities.setCapability("VERSION", VERSION);
        capabilities.setCapability("deviceName", deviceName);
        capabilities.setCapability("platformName", platformName);
        capabilities.setCapability("app", appPath);
        capabilities.setCapability("appPackage", appPackage);
        capabilities.setCapability("appActivity", appActivity);
        capabilities.setCapability("newCommandTimeout", 60 * 10);
        try {
            driver = new RemoteWebDriver(new URL("http://" + appiumServerIP + ":" + appiumServerPort + "/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        sleep(5);
        System.out.println("Step : Enter userName = " + userName);
        driver.findElement(By.id("com.rockers.ticketing:id/username")).sendKeys(userName);

        sleep(2);
        System.out.println("Step : Enter password = " + password);
        driver.findElement(By.id("com.rockers.ticketing:id/password")).sendKeys(password);

        sleep(2);
        System.out.println("Step : Click on login button");
        driver.findElement(By.id("com.rockers.ticketing:id/button_login")).click();

        sleep(20);
        System.out.println("Step : Exit App");
        driver.quit();
    }

    public static void sleep(int sec) {
        try {
            Thread.sleep(1000 * sec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String getAbsolutePath(String fileName) {
        File f = new File(fileName);
        return f.getAbsolutePath();
    }

}
